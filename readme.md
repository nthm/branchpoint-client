## Example client for Branchpoint

Basically that means it needs to be HTTP-only when behind a proxy, use an
external metrics service, and be hopefully be serverside renderable.

This isn't in the server side project because it adds a lot of complexity. When
trying to have them together it pulls in JSDOM, Node Sass, etc to what is
otherwise a minimal proxy server.

TODO: Dependencies:

- _bulma_: CSS strap. Up to 180kb. Written in Sass. Trying to do less work since
  the rest of the frontend components/state architecture is complex enough. Read
  notes in _client/style.css_
