const presets = [
  // Future: "@babel/preset-env" with "targets": { "esmodules": true }
];

const plugins = [
  "@babel/plugin-transform-typescript",
  "@pika/web/assets/babel-plugin.js",
];

module.exports = { presets, plugins };
