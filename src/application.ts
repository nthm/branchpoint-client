import { v } from 'voko';

const App = ({ version }: { version: any }, ...children: any[]) =>
  v('.ApplicationBoundary', [
    v('.Card', [
      v('h1.Card__Header', 'Hello from SSR\'d voko'),
      v('p', 'This is rendered using JSDOM and then merged into a base.html skeleton'),
    ]),
    v('.ApplicationBoundary__Version', `DEV ${version}`),
    ...children,
  ]);

export function render({ version = '0.0.0' }: { version: any }) {
  return v(App, { version });
}
