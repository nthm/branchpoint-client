Browser client

This is the frontend for Branchpoint itself and any internal services. Note this
isn't used for any external services behind the proxy.

TypeScript is compiled into an application for the browser. It may first be
prerendered server side. It's an isolated core supporting the notions of
components and services, with all client side rendering done in Voko.

There are no external dependencies, yet. Once there are, look into Babel to
replace/wrap the TypeScript compiler and use @pika/web (with Babel) to export
them as `<script type="module">`.

Expected modules:
  - Voko renderer
  - IndexedDB promise layer
  - Isolated core? Likely instead handled at compile time

Note that @pika/web is a wrap around Rollup, so using it could mean adding a lot
to the build pipeline. Trying to avoid Webpack-like development.
